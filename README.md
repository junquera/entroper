
# Entroper

Testing with a file made concatenating `.txt` files


```python
from entroper import main

main('/tmp/file.txt')
```

    4133 8390 5.071550172020524



![png](README_files/README_2_1.png)


And with the same file encrypted with `openssl`


```python
main('/tmp/file.txt.enc')
```

    4133 8390 7.9999948261470815



![png](README_files/README_4_1.png)

